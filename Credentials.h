struct Credentials {
  static const char *SSIDName;
  static const char *SSIDPassword;
  static const char *MQTTIPAddress;
  static const int   MQTTIPPort;
  static const char *MQTTUsername;
  static const char *MQTTPassword;
};


#include <Arduino.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include "SSD1306Wire.h"
#include "Credentials.h"

WiFiClient espClient;
PubSubClient mqtt(espClient);
//SSD1306Wire  display(0x3c, D3, D5);
SSD1306Wire  display(0x3c, 5, 4);

const char *mqttTopic = "esp8266/meter/1";
char priceBuffer[10];
float prprPrice = 0.0f;
float prevPrice = 0.0f;
float currPrice = 0.0f;

void messageFromMQTT(char* topic, byte* payload, unsigned int payloadLength);

void setup() {
  // Serial
  Serial.begin(115200);
  while (!Serial);
  // WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(Credentials::SSIDName, Credentials::SSIDPassword);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println(WiFi.localIP());
  // MQTT
  mqtt.setServer(Credentials::MQTTIPAddress, Credentials::MQTTIPPort);
  mqtt.setCallback(messageFromMQTT);
  // OLED
  display.init();
  display.flipScreenVertically();
  displayPrice();
}

void messageFromMQTT(char* topic, byte* payload, unsigned int payloadLength) {
  //unsigned long currentMillis = millis();
  //messageInterval = currentMillis - messageMillis;
  //messageMillis = currentMillis;

  DynamicJsonBuffer jsonBuffer(payloadLength);
  JsonObject &root = jsonBuffer.parseObject(payload);
  prprPrice = prevPrice;
  prevPrice = currPrice;
  currPrice = root["value"];
  Serial.println(priceBuffer);
  displayPrice();
}

void displayPrice() {
  display.clear();
  display.drawLine(0,0,127,0);
  display.drawLine(0,0,0,63);
  display.drawLine(127,63, 127, 0);
  display.drawLine(127,63, 0, 63);
  
  sprintf(priceBuffer, "%.1f", prprPrice);
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(2, 2, priceBuffer);
  
  sprintf(priceBuffer, "%.1f", prevPrice);
  display.setFont(ArialMT_Plain_16);  
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(126, 2, priceBuffer);
  
  sprintf(priceBuffer, "%.1f", currPrice);
  display.setFont(ArialMT_Plain_24);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 22, priceBuffer);
  display.display();
}

void connectMQTT() {
  if (mqtt.connect("ESP32OLEDMeter", Credentials::MQTTUsername, Credentials::MQTTPassword)) {
    if ( mqtt.subscribe(mqttTopic) ) {
      Serial.print("MQTT subscribed ");
      Serial.println(mqttTopic);
    } else {
      Serial.println("MQTT subscribe failed");
    }
  } else {
    Serial.print("MQTT connect failed ");
    Serial.println(mqtt.state());
  }
  delay(1000);  
}

void loop() {
  if ( ! mqtt.connected() ) {
    connectMQTT();
  }
  mqtt.loop();
  delay(1);
}



# ESP32_MQTT_Meter

A simple MQTT data sink that listens on "meter" for a three digit value and displays it on an OLED, in this case a Wemos ESP32 with integrated OLED.
